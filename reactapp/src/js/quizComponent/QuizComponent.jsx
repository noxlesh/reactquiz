import React from 'react';
import SettingsWindow from '../settingsWindow/SettingsWindow';
import ButtonComponent from '../../libraryComponents/ButtonComponent/ButtonComponent';
import './quizComponent.less';
import QuestionBox from "../questionBox/QuestionBox";
import {i18n, LangContext} from "../context/context";
import ResultWindow from "../resultWindow/ResultWindow";

export default class QuizComponent extends React.Component {
    constructor(props) {
        super(props);
        this.resultText = "";
        this.answerMap = new Map();
        this.state = {
            isSettingsOpen: false,
            isResultOpen: false,
            quizData: undefined,
            question: 0,
            username: localStorage.getItem("user")
        }
    }

    componentDidMount() {
        fetch("http://localhost:8099/quiz", {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
            }
        }).then((resp) => {
            if (resp.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                    resp.status);
                return;
            }
            return resp.json();
        }).then((data) => {
            console.log(data);
            this.setState(prevState => ({
                isSettingsOpen: prevState.isSettingsOpen,
                quizData: data,
            }))
        });
    }

    toggleSettingsWindow = () => {
        this.setState(prevState => ({ isSettingsOpen: !prevState.isSettingsOpen }));
    };

    toggleResultWindow = () => {
        this.setState(prevState => ({ isResultOpen: !prevState.isResultOpen }));
    };

    logOut = () => {
        this.props.logOutCallback();
    };

    nextQuestion = () => {
        if(this.state.question === this.state.quizData.length-1)
            this.setState({question: 0});
        else
            this.setState(prevState => ({question: prevState.question + 1}));
    };

    prevQuestion = () => {
        if(this.state.question === 0)
            this.setState(prevState => ({question: prevState.quizData.length - 1}));
        else
            this.setState(prevState => ({question: prevState.question - 1}));
    };

    answerChosen = (questionIndex, answerIndex ) => {
        this.answerMap.set(questionIndex, answerIndex)
    };

    result = () => {
        let score = 0;
        this.answerMap.forEach((val, key) => {
            if((this.state.quizData[key].answer - 1) === val)
                score++;
        });
        this.resultText = `${score} правильных ответа из ${this.state.quizData.length}`
        this.setState({isResultOpen:true})
    };

    render() {
        const { toggleSettingsWindow, toggleResultWindow, logOut, nextQuestion, prevQuestion, result, answerChosen } = this;
        const { isSettingsOpen, isResultOpen, quizData, question, username } = this.state;
        return (
            <LangContext.Consumer>
                { context => {
                    return (
                        <div className="page-wrapper">
                            <header className="page-wrapper-header">
                                <div className="nav">
                                    <ButtonComponent
                                        name={i18n[context.lang].settings}
                                        click={toggleSettingsWindow}
                                    />
                                    <ButtonComponent
                                        name={`${i18n[context.lang].logout} ${username}`}
                                        click={logOut}
                                    />
                                </div>

                            </header>
                            <main className="page-wrapper-content">
                                {isSettingsOpen &&
                                <SettingsWindow
                                    callbackClose={toggleSettingsWindow}
                                />
                                }
                                {isResultOpen &&
                                    <ResultWindow
                                        callbackClose={toggleResultWindow}
                                        text={this.resultText}
                                    />

                                }
                                <div className="question-pos">
                                    {
                                        quizData &&
                                        <span>{question + 1} {i18n[context.lang]['of']} {quizData.length}</span>
                                    }
                                </div>
                                <div className="quiz-question">
                                    {
                                        quizData &&
                                        quizData.map((questionData, i) =>
                                            <QuestionBox
                                                key={i}
                                                qIndex={i}
                                                text={questionData.text}
                                                show={(question === i)}
                                                answers={questionData.answers}
                                                answerChosen={answerChosen}
                                            />
                                        )
                                    }
                                    <ButtonComponent
                                        name={i18n[context.lang].prev}
                                        click={prevQuestion}/>
                                    <ButtonComponent
                                        name={i18n[context.lang].next}
                                        click={nextQuestion}/>
                                    <ButtonComponent
                                        name={i18n[context.lang].finish}
                                        click={result}/>
                                </div>
                            </main>
                            <footer className="page-wrapper-footer"/>
                        </div>
                    );
                }}
            </LangContext.Consumer>
        );
    }
}