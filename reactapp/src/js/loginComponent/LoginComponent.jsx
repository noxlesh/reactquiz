import React from 'react';
import ButtonComponent from "../../libraryComponents/ButtonComponent/ButtonComponent";
import './loginComponent.less'
import {LangContext,i18n} from '../context/context'


export default class LoginComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: "",
            password: ""
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    handleLoginSubmit = event => {
        event.preventDefault();
        fetch("http://localhost:8099/token", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                login: this.state.user,
                pass:  this.state.password,
            })
        }).then((resp) => {
            if (resp.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                    resp.status);
                return;
            }
            return resp.json()
        }).then((data) => {
            console.log(data);
            this.props.addUserCallback(this.state.user)
        });
    };

    

    render() {
        return (
            <LangContext.Consumer>
                {context =>{
                    return (
                    <div className="login-container">
                    <div className="login-box">
                        <div>
                            <input
                                type="text"
                                name="user"
                                value={this.state.user}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div>
                            <input
                                type="password"
                                name="password"
                                value={this.state.password}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div>
                            <ButtonComponent name={i18n[context.lang]['login']} backgroundColor="#acacac" click={this.handleLoginSubmit}/>
                        </div>
                    </div>
                </div>
                    );
                }}
            </LangContext.Consumer>
        )
    }
}