import React from 'react';
import './questionBox.less';

export default class QuestionBox extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            answerPos: -1
        }
    }

    changeHandler = (e,i) => {
        this.setState({answerPos: i});
        this.props.answerChosen(this.props.qIndex, i)
    };

    render() {
        const answerPos = this.state.answerPos;
        const {text, answers, show} = this.props;
        return (
            show &&
            <div className="question-container">
                <div className="question-container-text">{text}</div>
                <hr/>
                <div className="question-container-answers">
                    <form>
                        {answers.map((answer, i) =>
                            <div key={i}>
                                <input
                                    key={i}
                                    type="radio"
                                    onChange={(e) => {this.changeHandler(e,i)}}
                                    checked={answerPos === i}
                                />
                                {answer}
                            </div>
                        )}
                    </form>
                </div>
            </div>
        )
    }
}