import React from 'react';
import ReactDOM from 'react-dom';
import './resultWindow.less';
import PropTypes from 'prop-types';
import ButtonComponent from '../../libraryComponents/ButtonComponent/ButtonComponent';
import {i18n, LangContext} from "../context/context";

export default class ResultWindow extends React.Component {
    static propTypes = {
        callbackClose: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        this.portal = document.createElement('div');
        document.body.appendChild(this.portal);
    }

    componentWillUnmount() {
        document.body.removeChild(this.portal);
    }

    render() {
        const {callbackClose, text } = this.props;

        return ReactDOM.createPortal(
            <LangContext.Consumer>
                { context => {
                    return (
                        <div className="result-window-wrapper">
                            <div className="result-window">
                                <div className="result-window_content-container">
                                    <div>{i18n[context.lang].result}</div>
                                    <div>{text}</div>

                                </div>
                                <div className="result-window_button-container">
                                    <ButtonComponent
                                        name={i18n[context.lang].close}
                                        color={'blue'}
                                        click={callbackClose}
                                    />
                                </div>
                            </div>
                        </div>
                    );
                }}
            </LangContext.Consumer>,
            this.portal
        );
    }
}