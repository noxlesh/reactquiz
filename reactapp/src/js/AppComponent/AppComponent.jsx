import React from 'react';
import LoginComponent from "../loginComponent/LoginComponent.jsx";
import QuizComponent from '../quizComponent/QuizComponent.jsx';
import {LangContext} from "../context/context.jsx";

export default class AppComponent extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            logged: this.checkUser(),
            lang: localStorage['lang'] || 'en',
        };
    }

    addUser = (user) => {
        localStorage['user'] = user;
        this.setState({logged: true})
    };

    updateLang = (e) => {
        localStorage['lang'] = e.target.value;
        this.setState({lang: e.target.value});
    };

    remUser = () => {
        localStorage.removeItem("user");
        this.setState({logged: false})
    };

    checkUser = () => {
        return !(localStorage.getItem("user") === null);
    };

    render() {
        const isLogged = this.state.logged;
        return (
        <LangContext.Provider value={{lang:this.state.lang,updateLang:this.updateLang}}>
            {isLogged?(
                <QuizComponent logOutCallback={this.remUser}/>
            ):(
                <LoginComponent addUserCallback={this.addUser}/>
            )}
        </LangContext.Provider>
        )
    }
}