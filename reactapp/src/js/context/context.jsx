import React from "react";

export const LangContext = React.createContext();
export const i18n = {
    'en': {
        'lang':'English',
        'close':'Close',
        'settings':'Settings',
        'result':'Result',
        'login':'Login',
        'logout':'Logout',
        'of':'of',
        'next':'Next',
        'prev':'Prev',
        'finish':'Finish',
    },
    'ru': {
        'lang':'Русский',
        'close':'Закрыть',
        'settings':'Настройки',
        'result':'Результат',
        'login':'Войти',
        'logout':'Выйти',
        'of':'из',
        'next':'След.',
        'prev':'Пред.',
        'finish':'Завершить',
    },
};