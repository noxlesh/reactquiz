import React from 'react';
import ReactDOM from 'react-dom';
import './settingsWindow.less';
import PropTypes from 'prop-types';
import ButtonComponent from '../../libraryComponents/ButtonComponent/ButtonComponent';
import {i18n, LangContext} from "../context/context";

export default class SettingsWindow extends React.Component {
    static propTypes = {
        callbackClose: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        this.portal = document.createElement('div');
        document.body.appendChild(this.portal);
    }

    componentWillUnmount() {
        document.body.removeChild(this.portal);
    }

    render() {
        const {callbackClose } = this.props;

        return ReactDOM.createPortal(
            <LangContext.Consumer>
                { context => {
                    return (
                        <div className="settings-window-wrapper">
                            <div className="settings-window">
                                <div className="settings-window_content-container">
                                    <div>{i18n[context.lang].settings}</div>
                                    <select onChange={context.updateLang} value={context.lang}>
                                        {Object.entries(i18n).map(([key,val],i) =>
                                                <option
                                                    key={i}
                                                    value={key}
                                                >{val.lang}</option>

                                        )}
                                    </select>
                                </div>
                                <div className="settings-window_button-container">
                                    <ButtonComponent
                                        name={i18n[context.lang].close}
                                        color={'blue'}
                                        click={callbackClose}
                                    />
                                </div>
                            </div>
                        </div>
                    );
                }}
            </LangContext.Consumer>,
            this.portal
        );
    }
}