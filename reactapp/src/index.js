import ReactDOM from 'react-dom';
import React from 'react';
import AppComponent from './js/AppComponent/AppComponent.jsx'
import './styles/index.less';

// import logoSVG from 'assets/react-logo.svg';
// import logoPNG from 'assets/webpack.png';

ReactDOM.render(
   <AppComponent/>,
    document.getElementById('root')
);



