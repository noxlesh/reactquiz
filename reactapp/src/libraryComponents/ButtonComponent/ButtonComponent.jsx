import React from 'react';
import PropTypes from 'prop-types';
import './ButtonComponent.less';

const ButtonComponent = props => {
    const {
        name,
        click,
        color,
    } = props;

    return (
        <button
            className="btn"
            style={{backgroundColor:color}}
            onClick={click && click}>
            {name}
        </button>
    );
};

ButtonComponent.propTypes = {
    color: PropTypes.string,
    click: PropTypes.func.isRequired,
    name: PropTypes.string,
};

ButtonComponent.defaultProps = {
    name: 'ButtonComponent',
    color: 'grey',
};

export default ButtonComponent;