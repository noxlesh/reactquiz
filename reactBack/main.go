package main

import (
	"encoding/json"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"io/ioutil"
	"net/http"
)

type User struct {
	gorm.Model
	Name string `json:"user"`
	Password string `json:"password"`
}

func main() {
	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	db.AutoMigrate(&User{})

	//db.Create(&User{Name: "serg", Password: "serg"})

	e := gin.Default()
	e.Use(cors.Default())
	e.POST("/token", func(c *gin.Context) {

		body, _ := ioutil.ReadAll(c.Request.Body)
		var dataMap map[string]string
		json.Unmarshal(body, &dataMap)
		//fmt.Println(dataMap)
		var user User
		login, okLogin := dataMap["login"]
		pass, okPass := dataMap["pass"]
		if okLogin && okPass {
			db.Where("name = ?", login).First(&user)
			if user.Password == pass {
				c.JSON(http.StatusOK, gin.H{"status": "ok"})
				return
			}
			c.JSON(http.StatusUnauthorized, gin.H{"status": "unauthorized"})
			return
		}
		c.JSON(http.StatusBadRequest, gin.H{"status": "bad_request"})
	})

	e.GET("/quiz", func(c *gin.Context) {
		quiz :=  []map[string]interface{}{
				{
					"text":"Что такое React fiber?",
					"answers": []string {
						"Техноголия асинхронного рендера компонентов в react.js",
						"Модуль рисующий Оптоволокно",
						"Месседжер",
					},
					"answer": 1,
				},
				{
					"text":"Какой алиас можно использовать в место <React.Fragment></React.Fragment>?",
					"answers": []string {
						"<StrictMode>",
						"<body></body>",
						"<></>",
					},
					"answer": 3,
				},
				{
					"text":"Какой метод необходимо определить для перехвата ошибок?",
					"answers": []string {
						"willCatch",
						"try{...} catch() {...}",
						"componentDidCatch()",
					},
					"answer": 3,
				},
		}
		c.JSON(http.StatusOK, quiz)
	})

	e.Run(":8099")
}
